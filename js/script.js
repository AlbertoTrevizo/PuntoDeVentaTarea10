$(document).ready(function(){
  var total = 0;
  $("#btnAgregar").on("click", function(){
    if($(".txtproducto").val() == "" || $(".txtprecio").val() == ""){
      alert("Favor de llenar todos los campos!")
    }else{
      var producto = $(".txtproducto").val();
      var precio = parseInt($(".txtprecio").val());
      total += precio;
      $("tbody").append("<tr><td class='producto'>" + producto + "</td><td class='precio'>" + precio + "</td><td><a id='borrar'>X</a></td></tr>");
      $(".total").html("<b>$"+total+"</b>");
      $(".txtproducto").val("");
      $(".txtprecio").val("");
    }
  });

  $(document).on("click", "#borrar", function(){
    total -= parseInt($(this).closest("tr").find("td.precio").text());
    $(".total").html("<b>$"+total+"</b>");
    $(this).closest("tr").remove();
  });

  $(document).on("click", "#BorrarTodo", function(){
    $("tbody").empty();
    total=0;
    $(".total").html("<b>$"+total+"</b>");
  });
});
